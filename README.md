## Deploying

```bash
conda env create -f environment.yml
conda activate neural-xor
python generateDataset.py
python main.py
```

