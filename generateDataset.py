import numpy as np

n = 100000
x = np.random.randint(low=0, high=2, size=n).reshape(-1, 1)
y = np.random.randint(low=0, high=2, size=n).reshape(-1, 1)
z = np.logical_xor(x, y)

data = np.hstack([z, x, y])

np.savetxt("train.csv", data, delimiter=",")