import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

y_size = 1
x_size = 2

model = Sequential() # архитектура модели - последовательная
model.add(Dense(10, activation='relu', input_dim=x_size)) # входной слой, 8 нейронов
model.add(Dense(8, activation='relu')) # 1 скрытый слой, 4 нейрона
model.add(Dense(y_size, activation='sigmoid')) # 1 выходной слой, 1 нейронон с сигмоидальной функцией активации 
model.compile(loss='binary_crossentropy', optimizer='adam') # оптимизатор adam (https://keras.io/optimizers), функция потерь binary_crossentropy - для бинарной классификации
print(model.summary())

'''
Выбор количества нейронов и слоёв в модели в этой задаче ничем не обоснован
и служит только для демонстрации возможности добавления новых слоёв.
В действительности же достаточно только входного слоя, состоящего из
трёх нейронов и 1 выходного слоя с нейроном для получения результата:

model = Sequential()
model.add(Dense(3, activation='relu', input_dim=x_size))
model.add(Dense(y_size, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam')

Отмечу, что с  чуть большим, чем минимальным числом нейронов и слоёв модель обучается гораздо быстрее.
'''

df = np.genfromtxt('train.csv', delimiter=',')

x_data = df[:, y_size:] # входные данные
y_data = df[:, :y_size] # правильные ответы

# обучаем, 10 эпох
model.fit(x_data, y_data, batch_size=256, epochs=10)

s = 16 # демонстрация 16 первых значений
pred = model.predict(x_data[:s])

print(np.hstack([x_data[:s], np.round(pred)]))